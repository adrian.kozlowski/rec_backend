package pl.adriankozlowski.recBackend.exceptions;

public class GithubException extends RuntimeException {

    public GithubException(String message) {
        super(message);
    }
}
