package pl.adriankozlowski.recBackend.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorObject {

    private String message;
    private Instant time;
    private int code;
}
