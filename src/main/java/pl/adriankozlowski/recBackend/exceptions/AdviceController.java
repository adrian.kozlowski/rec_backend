package pl.adriankozlowski.recBackend.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

import java.time.Instant;

@RestControllerAdvice
public class AdviceController {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorObject> handleException(Exception e) {
        return ResponseEntity.status(500).body(new ErrorObject(e.getMessage(), Instant.now(), 500));
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<ErrorObject> handleGithubException(HttpClientErrorException e) {
        return ResponseEntity.status(e.getStatusCode()).body(new ErrorObject(e.getMessage(), Instant.now(), e.getRawStatusCode()));
    }
}
