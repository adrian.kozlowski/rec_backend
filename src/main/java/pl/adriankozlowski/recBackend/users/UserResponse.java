package pl.adriankozlowski.recBackend.users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    Long id;
    String login;
    String name;
    String type;
    String avatarUrl;
    String createdAt;
    BigDecimal calculations;
}
