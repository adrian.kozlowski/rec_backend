package pl.adriankozlowski.recBackend.users;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GithubResponse {

    private String login;
    private Long id;
    private String node_id;
    private String avatar_url;
    private String gravatar_id;
    private String url;
    private String html_url;
    private String followers_url;
    private String following_url;
    private String gists_url;
    private String starred_url;
    private String subscriptions_url;
    private String organizations_url;
    private String repos_url;
    private String events_url;
    private String received_events_url;
    private String type;
    private String site_admin;
    private String name;
    private String company;
    private String blog;
    private String location;
    private String email;
    private String hireable;
    private String bio;
    private String twitter_username;
    private Long public_repos;
    private Long public_gists;
    private Long followers;
    private Long following;
    @JsonProperty("created_at")
    private String createdAt; //": "2011-01-25T18:44:36Z",
    @JsonProperty("updated_at")
    private String updatedAt; //": "2021-06-22T14:27:36Z"


}
