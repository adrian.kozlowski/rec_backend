package pl.adriankozlowski.recBackend.users;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.adriankozlowski.recBackend.exceptions.GithubException;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@Slf4j
public class UserService {
    private final RestTemplate restTemplate;
    private final UserRepository userRepository;

    public UserService(RestTemplate restTemplate, UserRepository userRepository) {
        this.restTemplate = restTemplate;
        this.userRepository = userRepository;
    }

    /**
     * checks if users exists in database and ads request counter. Creates new user entry otherwise.
     *
     * @param login
     */
    private void updateDbCounter(String login) {
        Optional<UserEntity> optional = userRepository.findById(login);
        optional.ifPresentOrElse(userEntity -> {
            userEntity.setRequestCount(userEntity.getRequestCount() + 1L);
            userRepository.save(userEntity);
        }, () -> {
            userRepository.save(UserEntity.builder().requestCount(1L).login(login).build());
        });
    }

    /**
     * aggregates data to return info in response.
     * Calls github, fill object and updated counter in db
     *
     * @param login
     * @return full data object
     */
    public UserResponse getUserDetails(String login) {
        GithubResponse githubResponse = callGithub(login);
        UserResponse userResponse = generateUserResponse(login, githubResponse);
        updateDbCounter(login);
        return userResponse;
    }

    /**
     * creates {@link UserResponse} object
     *
     * @param login
     * @param githubResponse
     * @return
     */
    private UserResponse generateUserResponse(String login, GithubResponse githubResponse) {
        UserResponse userResponse = UserResponse.builder()
                .avatarUrl(githubResponse.getAvatar_url())
                .createdAt(githubResponse.getCreatedAt())
                .id(githubResponse.getId())
                .login(login)
                .name(githubResponse.getName())
                .type(githubResponse.getType())
                .calculations(doCalculations(githubResponse))
                .build();
        return userResponse;
    }

    /**
     * Does calculation required in task:
     * 6 / liczba_followers * (2 + liczba_public_repos).
     *
     * @param githubResponse
     * @return
     */
    private BigDecimal doCalculations(GithubResponse githubResponse) {
        Long followers = githubResponse.getFollowers();
        Long public_repos = githubResponse.getPublic_repos();
        log.info(" 6 / {} x ( 2 + {})", followers, public_repos);
        long addition = 2 + public_repos;
        log.info("followers={}, addition={}", followers, addition);
        return new BigDecimal(6f / followers.floatValue() * addition);
    }


    /**
     * Method calls gitHub and fetches data for login passed in param.
     *
     * @param login
     * @return Response object. Otherwise throws {@link GithubException}
     */
    public GithubResponse callGithub(String login) {
        log.info("Calling Github for login={} details", login);
        ResponseEntity<GithubResponse> forEntity = null;
        try {
            forEntity = restTemplate.getForEntity("https://api.github.com/users/{login}", GithubResponse.class, login);
        } catch (HttpClientErrorException e) {
            throw e;
        }
        return forEntity.getBody();
    }

}
