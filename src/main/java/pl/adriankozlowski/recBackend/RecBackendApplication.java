package pl.adriankozlowski.recBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecBackendApplication {

    /**
     * Runs the application.
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(RecBackendApplication.class, args);
    }

}
