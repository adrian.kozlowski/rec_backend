package pl.adriankozlowski.recBackend;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;
import pl.adriankozlowski.recBackend.users.GithubResponse;
import pl.adriankozlowski.recBackend.users.UserRepository;

import java.time.Instant;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class RecBackendApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RestTemplate restTemplate;
    @MockBean
    private UserRepository userRepository;

    @Test
    void contextLoads() {
    }

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        GithubResponse githubResponse = new GithubResponse();
        githubResponse.setPublic_repos(1L);
        githubResponse.setId(4444L);
        githubResponse.setAvatar_url("avatar url");
        githubResponse.setCreatedAt(Instant.now().toString());
        githubResponse.setName("name");
        githubResponse.setType("user");
        githubResponse.setFollowers(2L);

        String login = "fakeLogin";

        when(restTemplate.getForEntity("https://api.github.com/users/{login}", GithubResponse.class, login))
                .thenReturn(new ResponseEntity<GithubResponse>(githubResponse, HttpStatus.OK));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/users/fakeLogin"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"id\":4444,\"login\":\"fakeLogin\"")));
    }
}
