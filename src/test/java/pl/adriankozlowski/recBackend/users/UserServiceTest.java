package pl.adriankozlowski.recBackend.users;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.Random;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.mockito.Mockito.*;

class UserServiceTest {

    private Random random;
    private RestTemplate restTemplate;
    private UserRepository userRepository;
    private String login;
    private GithubResponse githubResponse;
    private UserEntity value;

    @BeforeEach
    public void setUpTests() {
        random = new Random();
        restTemplate = mock(RestTemplate.class);
        userRepository = mock(UserRepository.class);
        login = "fakeLogin";
        githubResponse = new GithubResponse();
        githubResponse.setPublic_repos(1L);
        githubResponse.setId(3333L);
        githubResponse.setAvatar_url("avatar url");
        githubResponse.setCreatedAt(Instant.now().toString());
        githubResponse.setName("name");
        githubResponse.setType("user");
        githubResponse.setFollowers(2L);

        value = new UserEntity(login, 1L);
    }

    @Test
    public void shouldGetUserDetails() {


        when(restTemplate.getForEntity("https://api.github.com/users/{login}", GithubResponse.class, login))
                .thenReturn(new ResponseEntity<GithubResponse>(githubResponse, HttpStatus.OK));
        when(userRepository.findById(login)).thenReturn(Optional.of(value));
        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);

        UserService userService = new UserService(restTemplate, userRepository);
        UserResponse fakeLogin = userService.getUserDetails("fakeLogin");
        verify(userRepository).save(userEntityArgumentCaptor.capture());


        UserEntity capture = userEntityArgumentCaptor.getValue();
        assertThat(fakeLogin).isNotNull();
        assertThat(fakeLogin.getCalculations()).isEqualTo(BigDecimal.valueOf(9));
        assertThat(capture.getRequestCount()).isEqualTo(2);
        assertThat(fakeLogin.getId()).isNotNull();
        assertThat(fakeLogin.getName()).isNotNull();
        assertThat(fakeLogin.getLogin()).isNotNull();
        assertThat(fakeLogin.getType()).isNotNull();
        assertThat(fakeLogin.getAvatarUrl()).isNotNull();
        assertThat(fakeLogin.getCreatedAt()).isNotNull();
    }

    @Test
    public void shouldGetUserDetailsAndCreateNewEntryInDb() {

        when(restTemplate.getForEntity("https://api.github.com/users/{login}", GithubResponse.class, login))
                .thenReturn(new ResponseEntity<GithubResponse>(githubResponse, HttpStatus.OK));
        when(userRepository.findById(login)).thenReturn(Optional.ofNullable(null));
        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);

        UserService userService = new UserService(restTemplate, userRepository);
        UserResponse fakeLogin = userService.getUserDetails("fakeLogin");
        verify(userRepository).save(userEntityArgumentCaptor.capture());


        UserEntity capture = userEntityArgumentCaptor.getValue();
        assertThat(fakeLogin).isNotNull();
        assertThat(fakeLogin.getCalculations()).isEqualTo(BigDecimal.valueOf(9));
        assertThat(capture.getRequestCount()).isEqualTo(1);
    }

    @Test
    public void shouldGetExceptionWhileCallGithub() {

        when(restTemplate.getForEntity("https://api.github.com/users/{login}", GithubResponse.class, login))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));
        when(userRepository.findById(login)).thenReturn(Optional.ofNullable(null));
        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);

        UserService userService = new UserService(restTemplate, userRepository);
        assertThatCode(() -> userService.getUserDetails("fakeLogin")).isInstanceOf(HttpClientErrorException.class);

    }
}
